# Example Spring Boot Web App

This project is a simple example Spring Boot Web Application.

Freemarker templates in `src/main/resources/templates` use the `@spring.message` Spring Macro to load text from `src/main/resources/messages.properties` or the user's locale equivalent.

## Lionbridge Content CLI

This project uses the Lionbridge COntent CLI to submit a file for translation, authorize the work and then download the results.

## Submitting Files for Translation

`submit` is Bash script that uses the Lionbridge Content CLI to:

- add the File `message.properties`
- add a Job requesting to translate the above file to French and German
- request a Quote for the above Job

## Authorizing the Quote

`authorize` is a Bash script thse use the Lionbridge Content CLI to:

- ensures the Quote is ready to be authorized
- authorizes the Quote with a PO Number

## Downloading Translated Files

`download` is a Bash script that:

- ensures the Job is completed
- downloads the translated files requested by the submit script above
- copies the files to a location recognized by Spring's Internationalization feature.

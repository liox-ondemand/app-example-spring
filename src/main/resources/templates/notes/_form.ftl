<div class="form-group">
    <label for="title"><@spring.message "notes.form.title"/></label>
    <@spring.bind 'note.title'/>
    <@spring.formInput "note.title", "class='form-control'" />
    <#list spring.status.errorMessages as error>
        <span id="helpBlock2" class="help-block">${error}</span>
    </#list>
</div>
<div class="form-group">
    <label for="content"><@spring.message "notes.form.content"/></label>
    <#assign htmlEscape = true in spring>
    <@spring.formTextarea "note.content",  "class='form-control' rows='8' cols='80'" />
    <#assign htmlEscape = false in spring>
    <p class="help-block"><a href="https://daringfireball.net/projects/markdown/syntax" target="_blank">Markdown syntax</a> <@spring.message "notes.form.markdown"/>.</p>
</div>

<@application>
    <h1>${note.title}</h1>

    <p>${note.contentHtml}</p>

    <p><a href="/notes/${note.id}/edit" class="btn btn-default">Edit Note</a></p>
</@application>

<@application>
    <h1><@spring.message "notes.edit.title"/></h1>

    <form action="/notes/${note.id}" method="POST">
        <input type="hidden" name="_method" value="PUT" />

        <#include '_form.ftl'>

        <button type="submit" class="btn btn-default"><@spring.message "notes.edit.submit"/></button>
    </form>
</@application>

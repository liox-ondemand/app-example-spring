<@application>
    <h1><@spring.message "notes.index.title"/></h1>

    <ul>
        <#list notes as note>
            <li><a href="/notes/${note.id}">${note.title}</a></li>
        </#list>
    </ul>

    <p><a href="/notes/new" class="btn btn-default"><@spring.message "notes.index.create"/></a></p>
</@application>

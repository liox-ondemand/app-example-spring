<@application>
    <h1><@spring.message "notes.new.title"/></h1>

    <form action="/notes" method="POST">

        <#include '_form.ftl'>

        <button type="submit" class="btn btn-default"><@spring.message "notes.new.submit"/></button>
    </form>
</@application>

<@application>
    <div class="jumbotron">
        <h1>NoteNation</h1>
        <p><@spring.message "home.index.welcome"/></p>
        <p><a href="/notes/new" class="btn btn-primary"><@spring.message "home.index.create"/></a></p>
    </div>
</@application>

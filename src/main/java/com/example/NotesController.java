package com.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@Controller
public class NotesController {
    @Autowired
    private NotesRepository notesRepository;

    @RequestMapping(path = "/notes", method = GET)
    public String notesIndex(Model model) {
        model.addAttribute("notes", notesRepository.findAllByOrderByTitleAsc());

        return "notes/index";
    }

    @RequestMapping(path = "/notes/new", method = GET)
    public String notesNew(Model model) {
        model.addAttribute("note", new Note());

        return "notes/new";
    }

    @RequestMapping(path = "/notes", method = POST)
    public String notesCreate(@Valid Note note, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("note", note);
            return "notes/new";
        }

        note = notesRepository.save(note);

        return "redirect:/notes/" + note.getId();
    }

    @RequestMapping(path = "/notes/{noteId}", method = GET)
    public String notesShow(@PathVariable("noteId") Integer noteId, Model model) {
        Note note = notesRepository.findOne(noteId);
        model.addAttribute("note", note);

        return "notes/show";
    }

    @RequestMapping(value = "/notes/{noteId}/edit", method = GET)
    public String notesEdit(@PathVariable("noteId") Integer noteId, Model model) {
        Note note = notesRepository.findOne(noteId);
        model.addAttribute("note", note);

        return "notes/edit";
    }

    @RequestMapping(value = "/notes/{noteId}", method = PUT)
    public String notesUpdate(@PathVariable("noteId") Integer noteId, @Valid Note note, BindingResult bindingResult, Model model) {
        note.setId(noteId);

        if (bindingResult.hasErrors()) {
            model.addAttribute("note", note);

            return "notes/edit";
        }

        notesRepository.save(note);

        return "redirect:/notes/" + note.getId();
    }
}

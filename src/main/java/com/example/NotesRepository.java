package com.example;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface NotesRepository extends CrudRepository<Note, Integer> {
    List<Note> findAllByOrderByTitleAsc();
}
